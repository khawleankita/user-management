import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  roles_arr = [
  { id: 0,name: 'admin'},
  { id: 1,name: 'user'},
  { id: 2,name: 'guest'}

]
userForm: FormGroup;

  constructor(private userService: UserService,public router: Router) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      user_name: new FormControl('', [Validators.required]),
      role: new FormControl('', [Validators.required]),
    });
  }

  reset() {
    this.userForm.reset();
  }
  onSubmit() {
    if(this.userForm.valid) {
      this.userService.createUser(this.userForm.value).subscribe((res: any)=> {
        if(res && res.success) {
          alert('User Added')
          this.router.navigate(['/']);
        } else {
          alert('User not saved')
        }

      })
    } else {
      alert('Kindly fill all the fields')
    }

  }
}
