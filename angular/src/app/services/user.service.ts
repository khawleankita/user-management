import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  
  getAllUsers() {
    return this.http.get(`http://localhost:3000/api/list`);
  }
  createUser(data) {
    return this.http.post(`http://localhost:3000/api/`, data);
  }
  deleteUser(id) {
    return this.http.delete(`http://localhost:3000/api/${id}`);
  }
  editUser(id, data) {
    return this.http.put(`http://localhost:3000/api/${id}`, data);
  }
  getUserByID(id) {
    return this.http.get(`http://localhost:3000/api/${id}`);
  }

}
