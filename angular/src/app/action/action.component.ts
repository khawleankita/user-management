import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit, ICellRendererAngularComp {
  public params: any;
  constructor() { }
  agInit(params: any): void {
    this.params = params;
  } 
  ngOnInit(): void {
  }
  openDelete() {
    console.log('tet');
    
  }


  public invokeParentMethod() {
    this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`);
  }
  refresh(): boolean {
    return false;
  }

  // openModal(template: TemplateRef<any>, modalconfig) {
  //   this.modalRef = this.modalService.show(template, modalconfig);
  // }


}

