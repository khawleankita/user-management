import { Component, ElementRef, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GridOptions } from 'ag-grid-community/dist/lib/entities/gridOptions';
import { GridReadyEvent } from 'ag-grid-community/dist/lib/events';
import { ActionComponent } from '../action/action.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  // @Input() rowData;
  gridApi: any;
  gridColumnApi: any;
  ToggledService: any;
  // @ViewChild('delete', { static: true } ) delete: ElementRef;
  @ViewChild('template', {static: true}) template: ElementRef;
  // @ViewChild("template", { read: typeof ElementRef, static: true  }) template: ElementRef;

  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, private userService: UserService, public router: Router) { }
  columnDefs = [
    {headerName: 'User Name', field: 'user_name',sortable: true },
    {headerName: 'Roles', field: 'role',sortable: true },
    {
      headerName: 'Edit',
      field: 'action_edit',
      cellRenderer: function(params) {
        return `
        <img style="width: 20px;height: auto;" src="../../assets/edit.png" (click)="openModal(template)">`
      } 
    },
    {
      headerName: 'Delete',
      field: 'action_delete',
      cellRenderer: function(params) {
        return `
        <img style="width: 20px;height: auto;" src="../../assets/delete.png" (click)="openModal(template)">`
      } 
    }

  ];
rowData ;
// = [
//     { user_name: 'Toyota', roles: 'Celica'},
//     { user_name: 'Ford', roles: 'Mondeo'},
// ];
frameworkComponents = {
  deleteAction: ActionComponent
};
  gridOptions: GridOptions = {
    suppressDragLeaveHidesColumns: true,
    animateRows: true,
    headerHeight: 45,
    context: {
      componentParent: this
    },  
    defaultColDef: {
      sortingOrder: ['asc', 'desc'],
      sortable: true,
      resizable: true
    }
  };
  ngOnInit(): void {
    this.getData()

  }
  getData() {
    this.userService.getAllUsers().subscribe(res=> {
      if(res) {
        this.rowData = res
      }
    })
  }
  gridReady(params: GridReadyEvent) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }

  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }

  onCellClick(event) {
    if (event.column.colId === 'action_delete' ) {
      const modalConfig = {
        class: 'modal-dialog-centered modal-sm modalCustom',
        ignoreBackdropClick: true
      };
      this.userService.deleteUser(event.data._id).subscribe(res=> {
        if(res) {
          alert('deleted');
          this.getData() 
        } 
      })
    } else if (event.column.colId === 'action_edit' ) {
      this.router.navigate([`/edit/${event.data._id}`]);
    }
  }

  create() {
    this.router.navigate(['/create']);
  }
  onQuickFilterChanged($event) {
    this.gridApi.setQuickFilter($event.target.value);
  }
  onClearSearchBox($event) {
    setTimeout(() => {
      if ($event.target.value.length === 0) {
        this.gridApi.setQuickFilter('');
      }
    }, 0);
  }

}