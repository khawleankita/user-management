import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  roles_arr = [
    { id: 0,name: 'admin'},
    { id: 1,name: 'user'},
    { id: 2,name: 'guest'}
  
  ]
  userForm: FormGroup;
  id;
  userData;
  constructor(private userService: UserService,public router: Router,private activatedRoute: ActivatedRoute) { }
  
  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
     if(params.get('id')) {
        this.id = params.get('id');
        this.getData();
     } else{
       alert('please provide an id for editing');
       this.router.navigate([`/`]);
     }
    });
      this.userForm = new FormGroup({
        user_name: new FormControl('', [Validators.required]),
        role: new FormControl('', [Validators.required]),
      });
  }
  getData() {
    if(this.id) {
      this.userService.getUserByID(this.id).subscribe((res: any)=> {
        if(res && res.length) {
          this.userForm.patchValue(res[0])
          this.userData = res[0];
        } else {
          alert("No such user found");
          this.router.navigate(['/']);
        }
      })
    } else {
      alert('Kindly fill all the fields')
    }
  }
    reset() {
      this.userForm.reset();
    }
    onSubmit() {
      if(this.userForm.valid) {
        this.userService.editUser(this.id, this.userForm.value).subscribe((res: any)=> {
          if(res && res.success) {
            alert('User updated')
            this.router.navigate(['/']);
          } else {
            alert('User not updated')
          }
  
        })
      } else {
        alert('Kindly fill all the fields')
      }
  
    }
  }
  