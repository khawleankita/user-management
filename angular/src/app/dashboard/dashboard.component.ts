import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  _opened: boolean = false;

  constructor(public router: Router) { }
 

  ngOnInit(): void {
  }
  _toggleSidebar() {
    this._opened = !this._opened;
  }
  redirect(){
    this.router.navigate(['/']);
  }
}
