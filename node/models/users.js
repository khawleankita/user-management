let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    user_name: String,
    role: String

},{timestamps: true} ,{collection: 'users'});


let userModel = mongoose.model("users", userSchema);

module.exports = { userModel };