var express = require('express');
var router = express.Router();
const UserController = require('../controllers/users')
const multer = require('multer');
var upload = multer();


router.get('/list', async (req, res) => {
    try {
        let users = new UserController(req.body,req.params);
        let response = await users.getList();
        res.send(response);

    } catch (error) { 
        res.status(error.statusCode || 500).send({ success: false, message: error.message});
    }
});
router.post('/', upload.none() , async (req, res) => {
    try {
        let users = new UserController(req.body);
        let response = await users.create();
        res.send(response);

    } catch (error) { 
        res.status(error.statusCode || 500).send({ success: false, message: error.message});
    }
});

router.put('/:id', async (req, res) => {
    try {
        let users = new UserController(req.body,req.params);
        let response = await users.editUser();
        res.send(response);

    } catch (error) { 
        res.status(error.statusCode || 500).send({ success: false, message: error.message});
    }
});
router.delete('/:id', upload.none() , async (req, res) => {
    try {
        let users = new UserController(req.body,req.params);
        let response = await users.deleteUser();
        res.send(response);

    } catch (error) { 
        res.status(error.statusCode || 500).send({ success: false, message: error.message});
    }
});
router.get('/:id', upload.none() , async (req, res) => {
    try {
        let users = new UserController(req.body,req.params);
        let response = await users.getUserById();
        res.send(response);

    } catch (error) { 
        res.status(error.statusCode || 500).send({ success: false, message: error.message});
    }
});

module.exports = router;