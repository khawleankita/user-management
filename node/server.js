'use strict';
let express = require('express');
let routes = require('./routes/index');
const cors = require('cors');
// let methodOverride = require('method-override');

let app = express();
let mongoose = require('mongoose');
var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(cors())
app.use('/api', routes);
global.CustomError = require('./utilities/custom_error');
const multer = require('multer');
let urlencodedParser=bodyParser.urlencoded({ extended: true });

// mongo
mongoose.connect("mongodb+srv://admin:root123@cluster0.dajbn.mongodb.net/userManagement?retryWrites=true&w=majority",{useNewUrlParser: true, useCreateIndex: true});
// mongoose.connect("mongodb+srv://meoqi:test123@cluster0.ajsid.mongodb.net/meoqi?retryWrites=true&w=majority",{useNewUrlParser: true, useCreateIndex: true});
// mongo


// firebase

const storage = multer.diskStorage({
  destination: './uploads/image',
});
const upload = multer({ storage: storage });

// app.get('/', async (req, res) => {
//   res.render('login', { banners: [] });
// });
// app.set('view engine', 'ejs');

app.listen( 3000,()=>{console.log(`server running on port ${process.env.PORT}`)});

module.exports = app;
