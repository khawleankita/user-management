
let UsersModel = require('../models/users');


class Users {
    constructor(payload, params=null) {
        
        this.payload = payload;
        this.params = params;    
        
    }
    
    async getList() {
        try {
            let response;
            response = await UsersModel.userModel.find({});
            if(response) {
                return response;
            } else {
                throw new CustomError('not working', 500 , 'fetchUsers');
            }          
        } catch (error) {
            throw new CustomError(error.message, error.statusCode, error.functionName);
        }
    }
    
    async getUserById() {
        try {
            let response;
            response = await UsersModel.userModel.find({_id: this.params.id});
            if(response) {
                return response;
            } else {
                throw new CustomError('not working', 500 , 'fetchUsers');
            }          
        } catch (error) {
            throw new CustomError(error.message, error.statusCode, error.functionName);
        }
    }
     async create() {
        try {
            let today = new Date();
 
            let obj = {
                role: this.payload.role,
                user_name:  this.payload.user_name,
                creation : today
            }
            let newUser = await UsersModel.userModel.create(obj);
            if(newUser) {
                return {
                    success: true,
                    newUser 
                }
            } else{
                throw new CustomError('Something went wrong.', 500, 'bookUsers');
            }

        } catch (error) {
            throw new CustomError(error.message, error.statusCode, error.functionName);
        }
    }


    async editUser() {
        try {

            let response;
            if(this.params.id) {

                let userObj;
                if(this.payload.user_name) {
                    userObj = {...userObj, ...{user_name: this.payload.user_name}}
                }
                if(this.payload.role) {
                    userObj = {...userObj, ...{role: this.payload.role}}
                }


                if(userObj){
                    response = await UsersModel.userModel.findByIdAndUpdate(this.params.id,userObj,{new:true , runValidators: true});
                    if(response) {
                        return {
                            success: true,
                            response
                        }
                    } else {
                        throw new CustomError('Failed to update user.', 500 , 'editUser');
                    }
                }else {
                    throw new CustomError('Add some data to user.', 500 , 'editUser');
                }
            } else {
                throw new CustomError('No user found', 500 , 'editUser');
            }

        } catch (error) {
            throw new CustomError(error.message, error.statusCode, error.functionName);
        }
    }

    async deleteUser() {
        try {
            let response;
            console.log(this.params.id);
            if(this.params.id) {
                response = await UsersModel.userModel.findByIdAndDelete(this.params.id,{ runValidators: true});
                if(response) {
                    return {
                        success: true,
                        response
                    }
                } else {
                    throw new CustomError('No user found', 500 , 'deleteUser');
                }
            } else {
                throw new CustomError('No id found', 500 , 'deleteUser');
            }
        } catch (error) {
            throw new CustomError(error.message, error.statusCode, error.functionName);
        }
    }
    
}

module.exports = Users;
